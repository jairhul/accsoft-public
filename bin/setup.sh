# only make directories and report when this script is called with initial
if [ "$1" == "--initial" ]; then
    INITIAL=1
elif [ "$1" == "--quiet" ]; then
    INITIAL=0
fi

BASEDIR=`pwd`
INSTALLDIR=$BASEDIR/install
SRCDIR=$BASEDIR/src
BUILDDIR=$BASEDIR/build
LOGDIR=$BASEDIR/log


# number of cpus available for fastest compilation
if [ "$(uname)" == "Darwin" ]; then
    NCPU=`sysctl -n hw.ncpu` # for mac
else
    NCPU=`nproc` || NCPU=4   # else for anything - ie linux
fi

if [[ $INITIAL == 1 ]]; then
    echo "Base directory        " $BASEDIR
    mkdir -p $INSTALLDIR
    mkdir -p $INSTALLDIR/bin
    echo "Installation directory" $INSTALLDIR
    mkdir -p $SRCDIR
    echo "Source directory      " $SRCDIR
    mkdir -p $BUILDDIR
    echo "Build directory       " $BUILDDIR
    mkdir -p $LOGDIR
    echo "Log directory         " $LOGDIR
    echo "Number of allowed processes" $NCPU
fi

# export to path etc software builds so new builds can
# pickup the already installed software
export PATH=$INSTALLDIR/bin/:./:$PATH
export LD_LIBRARY_PATH=$INSTALLDIR/lib/
export PYTHONPATH=$INSTALLDIR/lib/
