#!/bin/bash

function install_package {
    if [ -z "$1" ]
    then
	echo "No package name supplied"
	exit 1
    fi

    source $SCRIPTDIR/setup_admin.sh
    source $SCRIPTDIR/setup.sh --quiet
    source $SCRIPTDIR/setup_gcc.sh
    
    PKGNAME=$1
    LOG=$LOGDIR/$PKGNAME.log
    
    echo "Installing $PKGNAME" | tee -a $LOG
    easy_install --prefix=$INSTALLDIR $PKGNAME >> $LOG 2>&1
    echo "$PKGNAME complete" | tee -a $LOG
}