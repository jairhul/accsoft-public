#!/bin/bash

source $SCRIPTDIR/setup_admin.sh
source $SCRIPTDIR/setup.sh
source $SCRIPTDIR/setup_gcc.sh

DLDURL=https://bootstrap.pypa.io
DLDFILE=get-pip.py
PKGNAME=pip
LOG=$LOGDIR/$PKGNAME.log

cd $BASEDIR

rm -rf $LOG
echo "Downloading $PKGNAME" | tee $LOG
wget --quiet $DLDURL/$DLDFILE
rm -rf ./src/$DLDFILE
mv $DLDFILE ./src/

echo "Setting up python PIP"
python ./src/$DLDFILE --install-option="--prefix=$INSTALLDIR" >> $LOG 2>&1

echo "Python PIP complete" | tee -a $LOG