#!/bin/bash

source $SCRIPTDIR/setup_admin.sh
source $SCRIPTDIR/setup.sh
source $SCRIPTDIR/setup_gcc.sh

echo $SCRIPTDIR
DLDFILE=madpackage_SLAC.tgz
PKGPATH=${SCRIPTDIR}/../distSrc
PKGNAME=mad8
LOG=$LOGDIR/$PKGNAME.log

cd $BASEDIR

rm -rf $LOG

echo "Unpacking $PKGNAME" | tee -a $LOG
rm -rf $BASEDIR/build/$PKGNAME
mkdir $BASEDIR/build/$PKGNAME
tar zxf $PKGPATH/$DLDFILE -C ./build/$PKGNAME --strip-components=1

echo `pwd` >> $LOG

#echo "Cmake $PKGNAME" | tee -a $LOG
#cmake -DCMAKE_CXX_LINK_FLAGS="-L$INSTALLDIR/lib64 -L$INSTALLDIR/lib -Wl,-rpath,$INSTALLDIR/lib64,-rpath,$INSTALLDIR/lib" -DCMAKE_INSTALL_PREFIX=$INSTALLDIR/ ../$PKGNAME/ >> $LOG 2>&1


#echo "Building $PKGNAME" | tee -a $LOG
#make -j$NCPU >> $LOG 2>&1

#echo "Install $PKGNAME" | tee -a $LOG
#make -j$NCPU install >> $LOG 2>&1
