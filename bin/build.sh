#!/bin/bash

export SCRIPTDIR=${0%/*}
echo "Script sources were found in " $SCRIPTDIR
source $SCRIPTDIR/setup_admin.sh

# setup variables, make appropriate directories
source $SCRIPTDIR/setup.sh --initial

$SCRIPTDIR/gcc.sh
$SCRIPTDIR/cmake.sh
$SCRIPTDIR/llvm.sh
$SCRIPTDIR/clang.sh
$SCRIPTDIR/python.sh
$SCRIPTDIR/libpng.sh
$SCRIPTDIR/freetype.sh
$SCRIPTDIR/fontconfig.sh
$SCRIPTDIR/libxml2.sh
$SCRIPTDIR/xerces3.sh
$SCRIPTDIR/root.sh
$SCRIPTDIR/clhep.sh
$SCRIPTDIR/geant4.sh
$SCRIPTDIR/zlib.sh ! Not sure about this
$SCRIPTDIR/bdsim.sh --initial
$SCRIPTDIR/madx.sh
$SCRIPTDIR/pythonpackagesetup.sh
$SCRIPTDIR/robdsim.sh
$SCRIPTDIR/bdsimpyutils.sh
$SCRIPTDIR/pythonsetuptools.sh
$SCRIPTDIR/pythonpip.sh

# install python pacakges using package manger pip
INITIAL=0 # unset this as we're sourcing install_package.sh which uses setup.sh
source $SCRIPTDIR/install_package.sh
install_package numpy
install_package scipy
install_package matplotlib
install_package ipython
install_package root_numpy
install_package fortranformat

$SCRIPTDIR/setup_accsoft.sh
