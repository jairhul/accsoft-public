#!/bin/bash

source $SCRIPTDIR/setup_admin.sh
source $SCRIPTDIR/setup.sh
source $SCRIPTDIR/setup_gcc.sh

DLDURL=http://prdownloads.sourceforge.net/libpng/
DLDFILE=libpng-1.6.21.tar.gz
PKGNAME=libpng
LOG=$LOGDIR/$PKGNAME.log


##################################################
# libpng
##################################################
cd $BASEDIR
echo "Downloading $PKGNAME" | tee $LOG
wget --quiet $DLDURL/$DLDFILE?download
mv $DLDFILE?download ./src/$DLDFILE

echo "Unpacking $PKGNAME" | tee -a $LOG
rm -rf $BASEDIR/build/$PKGNAME
mkdir $BASEDIR/build/$PKGNAME
tar zxf $BASEDIR/src/$DLDFILE -C ./build/$PKGNAME --strip-components=1


rm -rf $BASEDIR/build/$PKGNAME-build
mkdir $BASEDIR/build/$PKGNAME-build
cd $BASEDIR/build/$PKGNAME-build

echo "Configuring $PKGNAME" | tee -a $LOG
../$PKGNAME/configure --prefix=$INSTALLDIR >> $LOG 2>&1

echo "Building $PKGNAME" | tee -a $LOG
make -j$NCPU >> $LOG 2>&1

echo "Installing $PKGNAME" | tee -a $LOG
make install >> $LOG 2>&1
