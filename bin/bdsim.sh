#!/bin/bash

if [ "$1" == "--initial" ]; then
    INITIAL=1
else
    INITIAL=0
fi

source $SCRIPTDIR/setup_admin.sh
source $SCRIPTDIR/setup.sh
source $SCRIPTDIR/setup_gcc.sh

PKGNAME=bdsim
LOG=$LOGDIR/$PKGNAME.log

cd $BASEDIR

rm -rf $LOG

echo "Cloning $PKGNAME" | tee -a $LOG
cd $BASEDIR/build/
if [[ $INITIAL == 1 ]]; then
    git clone https://bitbucket.org/jairhul/bdsim.git
fi
cd bdsim
git checkout develop
git pull
git submodule init
git submodule update

rm -rf $BASEDIR/build/$PKGNAME-build
mkdir $BASEDIR/build/$PKGNAME-build
cd $BASEDIR/build/$PKGNAME-build

echo `pwd` >> $LOG

echo "Cmake $PKGNAME" | tee -a $LOG
cmake -DCMAKE_CXX_LINK_FLAGS="-L$INSTALLDIR/lib64 -L$INSTALLDIR/lib -Wl,-rpath,$INSTALLDIR/lib64,-rpath,$INSTALLDIR/lib" -DCMAKE_INSTALL_PREFIX=$INSTALLDIR/ ../$PKGNAME/ >> $LOG 2>&1


echo "Building $PKGNAME" | tee -a $LOG
make -j$NCPU >> $LOG 2>&1

echo "Install $PKGNAME" | tee -a $LOG
make -j$NCPU install >> $LOG 2>&1

echo "Installing root macros" | tee -a $LOG
mkdir -p $INSTALLDIR/macros
cp $BASEDIR/build/$PKGNAME/analysis/rootlogon.C $INSTALLDIR/macros/.