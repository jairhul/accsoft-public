#!/bin/bash

source $SCRIPTDIR/setup_admin.sh
source $SCRIPTDIR/setup.sh
source $SCRIPTDIR/setup_gcc.sh
source $SCRIPTDIR/pythonpackagesetup.sh

PKGNAME=robdsim
LOG=$LOGDIR/$PKGNAME.log
echo $LOG

cd $BASEDIR

rm -rf $LOG

echo "Cloning $PKGNAME" | tee -a $LOG
cd $BASEDIR/build/
git clone https://bitbucket.org/jairhul/robdsim.git
cd robdsim

rm -rf $BASEDIR/build/$PKGNAME-build
mkdir $BASEDIR/build/$PKGNAME-build
cd $BASEDIR/build/$PKGNAME-build

echo `pwd` >> $LOG

echo "Cmake $PKGNAME" | tee -a $LOG
cmake -DCMAKE_CXX_LINK_FLAGS="-L$INSTALLDIR/lib64 -L$INSTALLDIR/lib -Wl,-rpath,$INSTALLDIR/lib64,-rpath,$INSTALLDIR/lib" -DCMAKE_INSTALL_PREFIX=$INSTALLDIR/ ../$PKGNAME/ >> $LOG 2>&1

echo "Building $PKGNAME" | tee -a $LOG
make -j$NCPU >> $LOG 2>&1

echo "Install $PKGNAME" | tee -a $LOG
make -j$NCPU install >> $LOG 2>&1

# copy a subcopy of robdsim .so for python
cp -R robdsim $PYTHONPKGDIR/.