#!/bin/bash

source $SCRIPTDIR/setup_admin.sh
source $SCRIPTDIR/setup.sh
source $SCRIPTDIR/setup_gcc.sh

DLDURL=http://madx.web.cern.ch/madx/releases/last-pro
DLDFILE=madx-linux64
PKGNAME=madx
LOG=$LOGDIR/$PKGNAME.log

cd $BASEDIR

rm -rf $LOG
echo "Downloading $PKGNAME" | tee $LOG
wget --quiet $DLDURL/$DLDFILE
mv $DLDFILE $INSTALLDIR/bin/
chmod ag+rx $INSTALLDIR/bin/$DLDFILE
ln -s $INSTALLDIR/bin/$DLDFILE $INSTALLDIR/bin/madx

