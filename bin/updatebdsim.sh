#!/bin/bash

export SCRIPTDIR=${0%/*}
echo "Script sources were found in " $SCRIPTDIR
source $SCRIPTDIR/setup_admin.sh

# setup variables
source $SCRIPTDIR/setup.sh

# note, bdsim.sh is executed without --initial flag here
$SCRIPTDIR/bdsim.sh
$SCRIPTDIR/robdsim.sh
$SCRIPTDIR/bdsimpyutils.sh

echo "Please source " $INSTALLDIR/bin/accsoft.sh "to use this software in future"
