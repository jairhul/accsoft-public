#!/bin/bash

source $SCRIPTDIR/setup_admin.sh
source $SCRIPTDIR/setup.sh
source $SCRIPTDIR/setup_gcc.sh

LOG=$LOGDIR/pythonpackages.log

rm -rf $LOG

PYTHONPKGDIR=$INSTALLDIR/lib/python2.7/site-packages
mkdir -p $PYTHONPKGDIR
echo "Python package dir: $PYTHONPKGDIR" | tee -a $LOG