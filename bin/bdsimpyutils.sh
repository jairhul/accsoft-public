#!/bin/bash

source $SCRIPTDIR/setup_admin.sh
source $SCRIPTDIR/setup.sh
source $SCRIPTDIR/pythonpackagesetup.sh

PKGNAME=bdsimpyutils
LOG=$LOGDIR/$PKGNAME.log

cd $PYTHONPKGDIR

echo `pwd` >> $LOG

# clone each utility - these are all on master branch so a straight clone is enough
echo "Cloning pymad8" | tee -a $LOG
git clone https://bitbucket.org/jairhul/pymad8.git

echo "Cloning pymadx" | tee -a $LOG
git clone https://bitbucket.org/jairhul/pymadx.git

echo "Cloning pybdsim" | tee -a $LOG
git clone https://bitbucket.org/jairhul/pybdsim.git