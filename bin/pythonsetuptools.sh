#!/bin/bash

source $SCRIPTDIR/setup_admin.sh
source $SCRIPTDIR/setup.sh
source $SCRIPTDIR/setup_gcc.sh
source $SCRIPTDIR/pythonpackagesetup.sh


DLDURL=https://pypi.python.org/packages/45/5e/79ca67a0d6f2f42bfdd9e467ef97398d6ad87ee2fa9c8cdf7caf3ddcab1e
DLDFILE=setuptools-23.0.0.tar.gz
PKGNAME=setuptools
LOG=$LOGDIR/$PKGNAME.log

cd $BASEDIR

rm -rf $LOG
echo "Downloading $PKGNAME" | tee $LOG
wget --quiet $DLDURL/$DLDFILE
rm -rf ./src/$DLDFILE
mv $DLDFILE ./src/
rm -rf $BASEDIR/build/$PKGNAME
mkdir $BASEDIR/build/$PKGNAME

echo "Unpacking $PKGNAME" | tee -a $LOG
tar zxf $BASEDIR/src/$DLDFILE -C ./build/$PKGNAME --strip-components=1
cd $BASEDIR/build/$PKGNAME

echo "Building $PKGNAME" | tee -a $LOG
python setup.py install --prefix $INSTALLDIR >> $LOG 2>&1

echo "Python setup tools complete" | tee -a $LOG