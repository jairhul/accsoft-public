#!/bin/bash

source $SCRIPTDIR/setup.sh

# prepare a env script for this specific build
# the paths are relative to the bin directory where the script will live.
# these are dynamic so the setup is portable
touch $INSTALLDIR/bin/accsoft.sh
cat << EOF > $INSTALLDIR/bin/accsoft.sh
#!/bin/bash
BASEDIR="\$( cd "\$( dirname "\${BASH_SOURCE[0]}" )" && pwd )"
export PATH=\$BASEDIR/:./:\$PATH
export LD_LIBRARY_PATH=\$BASEDIR/../lib
export PYTHONPATH=\$BASEDIR/../lib/
export CC=\$BASEDIR/gcc
export CCX=\$BASEDIR/g++
source \$BASEDIR/geant4.sh
source \$BASEDIR/thisroot.sh
alias pylab="ipython --pylab --colors=\"LightBG\""
EOF
chmod ag+x $INSTALLDIR/bin/accsoft.sh

echo "Please source " $INSTALLDIR/bin/accsoft.sh "to use this software in future"

