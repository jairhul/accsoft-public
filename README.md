# RHUL JAI Accelerator Software - Public #

Series of scripts by Stewart Boogert to create a complete environment for the majority of our accelerator software including Python, Geant4 & BDSIM.

## Included packages

GCC 4.9.3

CMake 3.5.2

LLVM 3.7.0

Clang 3.7.0

Python 2.7.11

LibPNG 1.6.21

FreeType 2.6.3

FontConfig 2.11.95

LibXML2 2.9.4

Xerces 3.1.3

CERN ROOT 6.06.04

CLHEP 2.3.3.0

Geant4 4.10.02.p02

BDSIM develop branch

CERN MADX - last-pro

Python setuptools 23

Python PIP - latest

## Python packages (all latest from website)

numpy

scipy

matplotlib

ipython

root_numpy

fortranformat

## BDSIM utilities

pybdsim

pymadx

pymad8

robdsim